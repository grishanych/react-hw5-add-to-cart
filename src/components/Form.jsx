import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import styles from './styles/Form.module.scss'
import {PatternFormat } from 'react-number-format';

const validationSchema = Yup.object().shape({
  name: Yup.string().required('Name is required'),
  lastname: Yup.string().required('Lastname is required'),
  age: Yup.number().required('Age is required').min(1, 'Age must be at least 1').max(100, 'Age must be less than or equal to 100'),
  address: Yup.string().required('Address is required'),
  phone: Yup.string().required('Phone is required'),
});

const CheckoutForm = () => {
  const initialValues = {
    name: '',
    lastname: '',
    age: '',
    address: '',
    phone: '',
  };

  const handleSubmit = (values) => {
    console.log('Data from form:', values);
    localStorage.removeItem('cartCount');
    localStorage.removeItem('selectedProductsInCart');

  };

  return (
    <div>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        <Form className={styles.form}>
          <div  className={styles.inputField}>
            <label htmlFor="name">Name</label>
            <Field type="text" id="name" name="name" className={styles.input} placeholder="Enter your name" />
            <ErrorMessage name="name" component="div" className={styles.error} />
          </div>
          <div className={styles.inputField}>
            <label htmlFor="lastname">Lastname</label>
            <Field type="text" id="lastname" name="lastname" className={styles.input} placeholder="Enter your lastname"/>
            <ErrorMessage name="lastname" component="div" className={styles.error} />
          </div>
          <div className={styles.inputField}>
            <label htmlFor="age">Age</label>
            <Field type="number" id="age" name="age" className={styles.input} placeholder="Enter your age"/>
            <ErrorMessage name="age" component="div" className={styles.error} />
          </div>
          <div className={styles.inputField}>
            <label htmlFor="address">Address</label>
            <Field type="text" id="address" name="address" className={styles.input} placeholder="Enter your address"/>
            <ErrorMessage name="address" component="div" className={styles.error} />
          </div>
          <div className={styles.inputField}>
            <label htmlFor="phone">Phone</label>
            <Field name="phone">
              {({ field }) => (
                <PatternFormat
                  {...field}
                  format="(###)###-##-##"
                  mask="_"
                  placeholder="Enter your phone"
                  className={styles.input}
                />
              )}
            </Field>
            <ErrorMessage name="phone" component="div" className={styles.error} />
          </div>
          <button type="submit" className={styles.input}>Checkout</button>
        </Form>
      </Formik>
    </div>
  );
};

export default CheckoutForm;
